package com.backend.projeto.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.backend.projeto.entity.Usuario;
import com.backend.projeto.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public Usuario findById(Long id) {
		return usuarioRepository.findById(id).orElse(null);
	}

	public Page<Usuario> findAll(PageRequest pageable) {
		return usuarioRepository.findAll(pageable);
	}

	public Usuario save(Usuario user) {
		return usuarioRepository.save(user);
	}

	public void edit(Usuario user, Long id) {
		Usuario usuario = findById(id);
		usuario.setEmail(user.getEmail());
		usuario.setNome(user.getNome());
		usuario.setPassword(user.getPassword());
		save(usuario);
	}

	public void deleteById(Long id) {
		usuarioRepository.deleteById(id);
	}

}
