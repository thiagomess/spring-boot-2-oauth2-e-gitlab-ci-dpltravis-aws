package com.backend.projeto.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

import com.backend.projeto.service.MyUserDetailService;

//Configurações do Resource Server: Servidor que hospeda os recursos a serem acessados. É quem recebe as requisições. É quem expõe a API que queremos acessar;
@Configuration
@EnableResourceServer
public class OAuth2ServerConfiguration extends ResourceServerConfigurerAdapter {

	private static final String RESOURCE_ID = "restService";

	@Override
	public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
		resources.resourceId(RESOURCE_ID);
	}

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.logout().invalidateHttpSession(true)
					.clearAuthentication(true)
					.and().authorizeRequests()
					.antMatchers(HttpMethod.OPTIONS, "/**").permitAll().anyRequest().fullyAuthenticated();
	}

	//Configurações do Authorization Server: Servidor que gera tokens de acesso, permite que o Client acesse os recursos, que o Resource Owner permitiu, com o nível de acesso que o Resource Owner especificou.
	@Configuration
	@EnableAuthorizationServer
	protected static class AuthorizationServerConfiguration extends AuthorizationServerConfigurerAdapter {

		private TokenStore tokenStore = new InMemoryTokenStore();

		@Qualifier("authenticationManagerBean")
		@Autowired
		private AuthenticationManager authenticationManager;

		@Autowired
		private MyUserDetailService userDetailService;

		@Autowired
		private PasswordEncoder passwordEncoder;

		
		//Armazenamento do token em memoria
		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
			endpoints.tokenStore(this.tokenStore)
					.authenticationManager(this.authenticationManager)
					.userDetailsService(userDetailService);
		}
		
		
		//Configuraçao dos clients que terão acesso
		//Tipos de GRANT:
		//Authorization_Code - para aplicativos em execução em um servidor Web, aplicativos móveis e baseados em navegador
		//Password - para fazer login com um nome de usuário e senha 
		//Client credentials - para acesso ao aplicativo sem a presença de um usuário
		//refresh token - Atualiza o token
		@Override
		public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
			clients.inMemory().withClient("client")
					.authorizedGrantTypes("client_credentials","password", "authorization_code", "refresh_token")
					.scopes("all").refreshTokenValiditySeconds(300000).resourceIds(RESOURCE_ID)
					.secret(passwordEncoder.encode("123")).accessTokenValiditySeconds(90)
					.and()
					.withClient("client2")
					.authorizedGrantTypes("password", "authorization_code", "refresh_token")
					.scopes("all").refreshTokenValiditySeconds(300000).resourceIds(RESOURCE_ID)
					.secret(passwordEncoder.encode("1234")).accessTokenValiditySeconds(120);
		}

		@Bean
		@Primary
		public DefaultTokenServices tokeServices() {
			DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
			defaultTokenServices.setSupportRefreshToken(true);
			defaultTokenServices.setTokenStore(this.tokenStore);
			return defaultTokenServices;

		}

	}
}
