package com.backend.projeto.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.backend.projeto.entity.Role;
import com.backend.projeto.entity.Usuario;
import com.backend.projeto.enums.Perfil;
import com.backend.projeto.repository.RoleRepository;
import com.backend.projeto.repository.UsuarioRepository;

//Classe que inicializa os dados no banco de dados
@Configuration
public class DadosInicializacao implements ApplicationListener<ContextRefreshedEvent>{

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		List<Usuario> listUsuario = usuarioRepository.findAll();
		
		if (listUsuario.isEmpty()) {
			this.criaUsuario("Thiago", "thiagogomes19@hotmail.com", passwordEncoder.encode("123"), Perfil.ALUNO);
			this.criaUsuario("Thiago Silva", "admin", passwordEncoder.encode("123"), Perfil.ADMIN);
		}
		
	}
	
	public void criaUsuario(String nome, String email, String password, Perfil perfil) {
		
		Role role = new Role();
		role.setName(perfil.getDescricao());
		roleRepository.save(role);
		
		
		Usuario usuario = new Usuario(nome, email, password, Arrays.asList(role));
		usuarioRepository.save(usuario);
		
	}
	

}
