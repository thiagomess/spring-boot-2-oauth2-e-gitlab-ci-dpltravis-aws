package com.backend.projeto.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.backend.projeto.entity.Usuario;
import com.backend.projeto.service.UsuarioService;

@RestController
@RequestMapping("user")
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;

	@Secured({"ROLE_ADMIN"})
	@GetMapping
	public Page<Usuario> list(@RequestParam("page") Integer page, @RequestParam("size") Integer size) {
		PageRequest pageable = PageRequest.of(page, size);
		return usuarioService.findAll(pageable);
	}

	@Secured({"ROLE_ADMIN", "ROLE_ALUNO"})
	@PostMapping
	public ResponseEntity<Void> save(@RequestBody Usuario user) {
		Usuario usuario = usuarioService.save(user);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(usuario.getId())
				.toUri();
		return ResponseEntity.created(uri).build();
	}

	@Secured({"ROLE_ADMIN", "ROLE_ALUNO"})
	@PutMapping(value = "/{id}")
	public ResponseEntity<Void> edit(@RequestBody Usuario user, @PathVariable Long id) {
		usuarioService.edit(user, id);
		return ResponseEntity.noContent().build();
	}

	@Secured({"ROLE_ADMIN", "ROLE_ALUNO"})
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		usuarioService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
	@Secured({"ROLE_ADMIN", "ROLE_ALUNO"})
	@GetMapping("/{id}")
	public ResponseEntity<Usuario> findById(@PathVariable Long id) {
		Usuario usuario = usuarioService.findById(id);
		return ResponseEntity.ok().body(usuario);
	}

}
