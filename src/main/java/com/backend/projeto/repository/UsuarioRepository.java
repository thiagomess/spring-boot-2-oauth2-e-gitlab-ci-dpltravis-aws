package com.backend.projeto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.backend.projeto.entity.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	Usuario findByEmail(String username);
}
